import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import roc_auc_score
from sklearn.metrics import r2_score
import seaborn as sns

dfTrain = pd.DataFrame(pd.read_csv('train.csv'))
dfTest = pd.DataFrame(pd.read_csv('test.csv'))
""" 
print(train.head())
print(train.info())
print(train.count())
print(train.isnull().sum())
 """
def make_plot(*args):
    numArgs = len(args)
    for i in range(numArgs):
        survived = dfTrain[dfTrain['Survived'] == 1][args[i]].value_counts()
        died = dfTrain[dfTrain['Survived'] == 0][args[i]].value_counts()
        df = pd.DataFrame([survived, died])
        df.index = ['Survived', 'Died']
        df.plot(kind='bar', stacked=True, grid=True, figsize=(5,10))
    plt.show()
#make_plot('Pclass','SibSp','Age')

""" fareBox = sns.boxplot(y = 'Fare', data=dfTrain)
fareBox = sns.pairplot(data = dfTrain, hue='Survived')
fareBox.set_title("Fare Distribuition")
plt.show()  """


""" #sum of NaN values:

print(dfTrain.isna().sum())
print('----------')
print(dfTest.isna().sum()) """

dfTrain['Age'] = dfTrain['Age'].fillna(dfTrain['Age'].mean())
dfTest['Age'] = dfTest['Age'].fillna(dfTest['Age'].mean())
dfTest['Fare'] = dfTest['Fare'].fillna(dfTest['Fare'].mean())
#dfTrain['Cabin'] = dfTrain['Cabin'].fillna("?")
#dfTest['Cabin'] = dfTest['Cabin'].fillna("?")
dfTrain = dfTrain.dropna()

""" print(dfTrain.isna().sum())
print('----------')
print(dfTest.isna().sum()) """

sex = {'male':0, 'female':1}
dfTrain.loc[:, 'Sex'] = dfTrain['Sex'].map(sex)
dfTest.loc[:, 'Sex'] = dfTest['Sex'].map(sex)
#avoiding ValueError for the 'Name' column (also trying for 'Ticket' and 'Cabin):
dfTrain = dfTrain.drop(columns=['Name', 'Ticket', 'Cabin'], axis=1)
dfTest = dfTest.drop(columns=['Name', 'Ticket', 'Cabin'], axis=1)
#hot encoding for solving 'Embarked' nominal problem (https://www.kaggle.com/dietzschdaniel/my-titanic-approach-top-6)
dfTrain = pd.get_dummies(dfTrain, prefix_sep="__",
                              columns=['Embarked'])
dfTest = pd.get_dummies(dfTest, prefix_sep="__",
                              columns=['Embarked']) 

#excluding the 'Survived' column from X
X = dfTrain.loc[:, dfTrain.columns != 'Survived']
y = dfTrain['Survived'].values

""" print(dfTrain.head())
print(dfTest.head()) """
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
#testing models:
model = GaussianNB() #great candidate
#model = KNeighborsClassifier()
#model = RandomForestClassifier() 


model.fit(x_train, y_train)

predict = model.predict(dfTest)

#print(roc_auc_score(y_test, predict))
#print(predict)


gender_submission = pd.read_csv('gender_submission.csv')

print(gender_submission.head())

gender_submission['Survived'] = predict
gender_submission.to_csv('Titanic.csv', index=False)
print(gender_submission.head())
